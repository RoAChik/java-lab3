import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Антон on 11.12.2015.
 */
public class MainClient {

    public static void main(String[] args) {

        if (args.length != 5) throw new NullPointerException("Invalid args");
        try {
            InetAddress addr = InetAddress.getByName(args[0]);
            int port = Integer.parseInt(args[1]);
            String prefix = args[2];
            int threads = Integer.parseInt(args[3]);
            int numbers = Integer.parseInt(args[4]);
            ThreadPoolExecutor tpc = new ThreadPoolExecutor(numbers, numbers, Integer.MAX_VALUE,
                    TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

            for (int i = 0; i < threads; i++) {
                tpc.execute(new Client(i, addr, port, prefix,numbers));

            }
            tpc.shutdown();

        } catch (UnknownHostException e) {
                e.printStackTrace();
            }
    }
}
