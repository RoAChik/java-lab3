import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by Антон on 10.12.2015.
 */
public class Server implements Runnable {
    private DatagramSocket socket;
    private DatagramPacket in;
    private DatagramPacket out;
    public Server(DatagramSocket socket)
    {
        this.socket = socket;
        in = new DatagramPacket(new byte[9999], 9999);
    }
    public void run() {
        while (true) {
            try {
                socket.receive(in);
                String incoming = new String(in.getData(), in.getOffset(), in.getLength());
                System.out.println("Incoming: " + incoming);
                String response = "Hello, " + incoming;
                out = new DatagramPacket(response.getBytes(), response.getBytes().length, in.getAddress(), in.getPort());
                socket.send(out);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
    }
}
