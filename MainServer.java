import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Антон on 10.12.2015.
 */
public class MainServer {
    public static void main(String[] args) {
        if(args.length != 2) throw new NullPointerException("Illegal args");
        int port = Integer.parseInt(args[0]);
        int threads = Integer.parseInt(args[1]);
        ExecutorService tps =
                new ThreadPoolExecutor(10, 10, Integer.MAX_VALUE, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        DatagramSocket serverDatagramSocket;
        try {
            serverDatagramSocket = new DatagramSocket(port);
            for (int i = 0; i < threads; i++) {
                tps.execute(new Server(serverDatagramSocket));
                tps.shutdown();
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}
