import java.io.IOException;
import java.net.*;

/**
 * Created by Антон on 11.12.2015.
 */
public class Client implements Runnable {
    private int id;
    private int count;
    private InetAddress address;
    private int port;
    private int numbers;
    private String prefix;

    public Client(int id, InetAddress address, int port, String prefix,int numbers) {
        this.id = id;
        this.count = 0;
        this.address = address;
        this.port = port;
        this.prefix = prefix;
        this.numbers = numbers;
    }

    @Override
    public void run() {
        DatagramSocket clientDatagramSocket = null;
        while (true) {
            try {
                clientDatagramSocket = new DatagramSocket();
                String message = prefix+"_"+id+"_"+count;
                DatagramPacket out = new DatagramPacket(message.getBytes(), message.getBytes().length, address, port);
                DatagramPacket in = new DatagramPacket(new byte[9999], 9999);
                clientDatagramSocket.send(out);
                clientDatagramSocket.setSoTimeout(1000);
                clientDatagramSocket.receive(in);
                String response = new String(in.getData(), in.getOffset(), in.getLength());
                System.out.println("Response: " + response);
            } catch (SocketTimeoutException e)
            {
                count--;
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                count++;
                if(count == numbers) break;
            }
        }
        try {
            clientDatagramSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
